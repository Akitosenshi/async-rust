/// single threaded concurrency
pub mod task {
    use std::{
        cell::RefCell,
        future::Future,
        mem::forget,
        pin::Pin,
        sync::{
            atomic::AtomicU32,
            mpsc::{channel, Receiver, Sender, TryRecvError},
            Arc,
        },
        task::{Context, Poll, RawWaker, RawWakerVTable, Wake, Waker},
    };

    pub struct Scheduler {
        // TODO: maybe extract to a queue struct
        queue: Receiver<Arc<Task>>,
        sender: Option<Sender<Arc<Task>>>,
    }

    // TODO remove visibility for anywhere not needed
    pub struct Task {
        pub marker: u32,
        future: RefCell<Pin<Box<dyn Future<Output = ()> + 'static>>>,
        sender: Sender<Arc<Task>>,
    }

    impl Task {
        // following functions are used for RawWakerVTable
        /// clone the RawWaker
        unsafe fn vt_clone(ptr: *const ()) -> RawWaker {
            // create arc and clone to increase ref count, then forget both to keep ref count intact
            let arc: Arc<Task> = Arc::from_raw(ptr as *const Task);
            let clone = arc.clone();
            forget(arc);
            forget(clone);

            RawWaker::new(
                ptr,
                &RawWakerVTable::new(
                    Self::vt_clone,
                    Self::vt_wake,
                    Self::vt_wake_by_ref,
                    // clones should be dropped so that the ref count will get decreased
                    Self::vt_drop,
                ),
            )
        }
        /// wake the Task
        unsafe fn vt_wake(ptr: *const ()) {
            let arc: Arc<Task> = Arc::from_raw(ptr as *const Task);
            // arc will be moved into wake() dropping it and decreasing ref count
            arc.wake();
        }
        /// wake the Task by reference
        unsafe fn vt_wake_by_ref(ptr: *const ()) {
            let arc: Arc<Task> = Arc::from_raw(ptr as *const Task);
            arc.wake_by_ref();
            // forget arc to avoid decreasing ref count
            forget(arc);
        }
        /// drop the Task
        unsafe fn vt_drop(ptr: *const ()) {
            let arc: Arc<Task> = Arc::from_raw(ptr as *const Task);
            drop(arc);
        }
        /// do nothing
        unsafe fn vt_noop(_: *const ()) {
            // do nothing
        }

        fn into_waker(self: &Arc<Self>) -> Waker {
            let ptr = Arc::as_ptr(self) as *const ();
            unsafe {
                Waker::from_raw(RawWaker::new(
                    ptr,
                    &RawWakerVTable::new(
                        Self::vt_clone,
                        Self::vt_wake,
                        Self::vt_wake_by_ref,
                        // original should not be dropped by waker to prevent accessing dropped
                        // memory when interacting with the corresponding task
                        Self::vt_noop,
                    ),
                ))
            }
        }
    }

    impl Wake for Task {
        fn wake(self: Arc<Self>) {
            let clone = self.clone();
            let _ = self.sender.send(clone);
        }
    }

    pub enum SchedulerState {
        Done,
        Disconnected,
    }

    static mut TASK_NUM: AtomicU32 = AtomicU32::new(0);

    impl Scheduler {
        pub fn new() -> Self {
            let (sender, queue) = channel();
            Self {
                queue,
                sender: Some(sender),
            }
        }

        /// closes this scheduler for new tasks.
        pub fn hang_up(&mut self) {
            self.sender.take();
        }

        pub fn spawn(&mut self, future: impl Future<Output = ()> + 'static) {
            let marker = unsafe { TASK_NUM.fetch_add(1, std::sync::atomic::Ordering::SeqCst) };
            if let Some(sender) = self.sender.take() {
                let task = Arc::new(Task {
                    // TODO: find a non unsafe way of doing this maybe?
                    marker,
                    future: RefCell::new(Box::pin(future)),
                    sender: sender.clone(),
                });
                match sender.send(task) {
                    Ok(_) => {
                        self.sender = Some(sender);
                    }
                    Err(_) => {
                    }
                }
            }
        }

        pub fn run(&mut self) -> SchedulerState {
            loop {
                match self.queue.try_recv() {
                    Ok(task) => {
                        let waker = task.into_waker();
                        let mut cx = Context::from_waker(&waker);
                        let poll_res = task.future.borrow_mut().as_mut().poll(&mut cx);
                        match poll_res {
                            Poll::Pending => {
                            }
                            Poll::Ready(val) => {
                            }
                        }
                    }
                    Err(err) => match err {
                        TryRecvError::Empty => {
                            return SchedulerState::Done;
                        }
                        TryRecvError::Disconnected => {
                            return SchedulerState::Disconnected;
                        }
                    },
                }
            }
        }
    }
}
