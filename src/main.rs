#![feature(waker_getters)]
use std::{future::Future, sync::Arc, thread::sleep, time::Duration};

use libasync::task::{Scheduler, SchedulerState};


// TODO: from tokio:
// struct Context {
//     /// Uniquely identifies the current thread
//     #[cfg(feature = "rt")]
//     thread_id: Cell<Option<ThreadId>>,

//     /// Handle to the runtime scheduler running on the current thread.
//     #[cfg(feature = "rt")]
//     current: current::HandleCell,

//     /// Handle to the scheduler's internal "context"
//     #[cfg(feature = "rt")]
//     scheduler: Scoped<scheduler::Context>,

//     #[cfg(feature = "rt")]
//     current_task_id: Cell<Option<Id>>,

//     /// Tracks if the current thread is currently driving a runtime.
//     /// Note, that if this is set to "entered", the current scheduler
//     /// handle may not reference the runtime currently executing. This
//     /// is because other runtime handles may be set to current from
//     /// within a runtime.
//     #[cfg(feature = "rt")]
//     runtime: Cell<EnterRuntime>,

//     #[cfg(any(feature = "rt", feature = "macros"))]
//     rng: Cell<Option<FastRand>>,

//     /// Tracks the amount of "work" a task may still do before yielding back to
//     /// the scheduler
//     budget: Cell<coop::Budget>,

//     #[cfg(all(
//         tokio_unstable,
//         tokio_taskdump,
//         feature = "rt",
//         target_os = "linux",
//         any(target_arch = "aarch64", target_arch = "x86", target_arch = "x86_64")
//     ))]
//     trace: trace::Context,
// }
//
// pub(crate) enum Handle {
//     #[cfg(feature = "rt")]
//     CurrentThread(Arc<current_thread::Handle>),

//     #[cfg(all(feature = "rt-multi-thread", not(target_os = "wasi")))]
//     MultiThread(Arc<multi_thread::Handle>),

//     #[cfg(all(tokio_unstable, feature = "rt-multi-thread", not(target_os = "wasi")))]
//     MultiThreadAlt(Arc<multi_thread_alt::Handle>),

//     // TODO: This is to avoid triggering "dead code" warnings many other places
//     // in the codebase. Remove this during a later cleanup
//     #[cfg(not(feature = "rt"))]
//     #[allow(dead_code)]
//     Disabled,
// }

// TODO: future and return value need to be Send; add trait bound
fn main() {
    std::thread::spawn(move || {
        let mut sched = Scheduler::new();
        sched.spawn(main_task(42, 5));
        sched.spawn(main_task(69, 10));
        let mut once = true;
        loop {
            match sched.run() {
                SchedulerState::Done => {
                    println!("all tasks done for now");
                    if once {
                        sched.spawn(main_task(1337, 3));
                        once = false;
                        sched.hang_up();
                    }
                }
                SchedulerState::Disconnected => {
                    println!("all senders closed!");
                    break;
                }
            }
        }
    });

    println!("continue main thread");
    sleep(Duration::from_secs(5));
    println!("exiting main thread");
}

struct MyFuture {
    tries: u32,
    after: u32,
    num: u32,
}

pub async fn main_task(num: u32, after: u32) {
    println!("new task: yield {} after {} polls: ", num, after);
    let f1 = MyFuture::new(num, after);
    let final_num = f1.await;
    println!("\tgot number: {}", final_num);
}

impl MyFuture {
    pub fn new(num: u32, after: u32) -> Self {
        Self {
            tries: 0,
            after,
            num,
        }
    }
}

impl Future for MyFuture {
    type Output = u32;
    fn poll(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        ) -> std::task::Poll<Self::Output> {
        let me = self.get_mut();
        if me.tries < me.after {
            me.tries += 1;
            let tmp = unsafe { Arc::from_raw(cx.waker().as_raw().data() as *const libasync::task::Task) };
            println!("\ttask {} polled {} times", tmp.marker, me.tries);
            std::mem::forget(tmp);
            sleep(Duration::from_millis(100));
            cx.waker().wake_by_ref();
            std::task::Poll::Pending
        } else {
            std::task::Poll::Ready(me.num)
        }
    }
}
